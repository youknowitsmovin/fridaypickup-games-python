/*
 Navicat MariaDB Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100136
 Source Host           : localhost:3306
 Source Schema         : pickup_games

 Target Server Type    : MariaDB
 Target Server Version : 100136
 File Encoding         : 65001

 Date: 21/08/2019 00:07:43
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;
-- ----------------------------
-- Table structure for statistic_player
-- ----------------------------
DROP TABLE IF EXISTS `statistic_player`;
CREATE TABLE `statistic_player`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(120) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nick_name` varchar(120) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_number` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `home_address` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `work_address` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `position` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alt_position` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `birth_date` date NULL DEFAULT NULL,
  `from_universities` varchar(120) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `class_year` varchar(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `back_number` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `height_in_centimeter` int(11) NULL DEFAULT NULL,
  `weight_in_kilogram` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 259 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of statistic_player
-- ----------------------------
INSERT INTO `statistic_player` VALUES (193, 'Abdul Hafiz Mugni Hafiz', 'Hafiz', 'abdulhafizmugni@gmail.com', '81299102191', 'Jakarta Timur', 'Denpasar Utara', 'Small Forward', 'Power Forward', NULL, 'Universitas Indonesia', '2008', '11', 179, 82);
INSERT INTO `statistic_player` VALUES (194, 'Agung Wicaksono', 'Wicaksono', 'agungw.23@gmail.com', '81806600191', 'Jakarta Pusat', 'Jakarta Pusat', 'Point Guard', 'Shooting Guard', NULL, 'Universitas Indonesia', '2007', '6', 170, 70);
INSERT INTO `statistic_player` VALUES (195, 'Ahdhi Thamus Adi', 'Adi', 'ahdhi.thanus@gmail.com', '81382199796', 'Slipi', 'Jakarta Barat', 'Center', 'Point Guard', NULL, 'Universitas Indonesia', '2008', '18', 169, 70);
INSERT INTO `statistic_player` VALUES (196, 'Akmal Lutfi Akmal / Akay', 'Akmal / Akay', 'akmalutfx@gmail.com', '81296952626', 'Jakarta Timur', 'Jakarta Pusat', 'Small Forward', 'Point Guard;Shooting', NULL, 'Universitas Indonesia', '2013', '12', 176, 88);
INSERT INTO `statistic_player` VALUES (197, 'Andi Haris Andi', 'Andi', 'ahrs26@gmail.com', '81310186388', 'Jakarta timur', 'Jayapura', 'Power Forward', 'Small Forward', NULL, 'Universitas Indonesia', '2010', '4', 180, 82);
INSERT INTO `statistic_player` VALUES (198, 'Andreas Christmanto Andreas', 'Andreas', 'andreas.christmanto@gmail.com', '81219166851', 'Duren sawit', 'Fatmawati', 'Power Forward', 'Small Forward;Center', NULL, 'Universitas Indonesia', '2006', '12', 177, 93);
INSERT INTO `statistic_player` VALUES (199, 'Andreas Rae Johanes Anes', 'Anes', 'andreasrjp@gmail.com', '82231998219', 'Jakarta Timur', 'Jakarta Selatan', 'Shooting Guard', 'Point Guard', NULL, 'Universitas Brawijaya', '2018', '3', 170, 80);
INSERT INTO `statistic_player` VALUES (200, 'Andree Siantoro Andree', 'Andree', 'andree_ps@yahoo.com', '81938917259', 'Jakarta Pusat', 'Jakarta Pusat', 'Small Forward', 'Power Forward', NULL, 'Universitas Indonesia', '2012', '2', 178, 90);
INSERT INTO `statistic_player` VALUES (201, 'Aradichi Prihandana Ara', 'Ara', 'ara.rookie@gmail.com', '81291443289', 'Jakarta Selatan', 'Ambon', 'Power Forward', 'Center', NULL, 'Universitas Indonesia', '2013', '9', 182, 80);
INSERT INTO `statistic_player` VALUES (202, 'Arief Rachman Kolep', 'Kolep', 'ariefkoleprachman@gmail.com', '87809848225', 'Bintaro', 'Cikarang', 'Point Guard', 'Shooting Guard', NULL, 'Universitas Indonesia', '2008', '7', 167, 63);
INSERT INTO `statistic_player` VALUES (203, 'Aulia Azhar Abdurachman Azay', 'Azay', 'aulia_azhar@yahoo.co.id', '81299019094', 'Jakarta Selatan', 'Jakarta Selatan', 'Center', 'Center', NULL, 'Universitas Indonesia', '2006', '1', 190, 120);
INSERT INTO `statistic_player` VALUES (204, 'bagus pradityo baba', 'baba', 'bagusp.tyo@gmail.com', '82213740713', 'Jakarta Timur', 'Jakarta selatan', 'Small Forward', 'Power Forward;Center', NULL, 'Universitas Indonesia', '2014', '13', 176, 75);
INSERT INTO `statistic_player` VALUES (205, 'Budi Mulia Budi', 'Budi', 'budimuliapanggabean@gmail.com', '81364549402', 'Tangerang', 'Cengkareng', 'Power Forward', 'Center', NULL, 'Universitas Indonesia', '2012', '5', 183, 86);
INSERT INTO `statistic_player` VALUES (206, 'Carlos William Carlos', 'Carlos', 'carlossswilliam@gmail.com', '8119392808', 'Tangerang', 'Jakarta pusat', 'Small Forward', 'Shooting Guard;Power', NULL, 'Universitas Pelita Harapan', '2014', '6', 178, 87);
INSERT INTO `statistic_player` VALUES (207, 'chadistira pranatyo dira', 'dira', 'dira_pranatyo@yahoo.com', '87781484389', 'jakarta selatan', 'jakarta pusat', 'Shooting Guard', 'Small Forward', NULL, 'Perbanas', '2007', '9', 182, 79);
INSERT INTO `statistic_player` VALUES (208, 'Chairul anam Anam', 'Anam', 'chairulanamm5@gmail.com', '85694232154', 'Jakarta pusat', 'Jakarta selatan', 'Point Guard', 'Shooting Guard', NULL, 'Universitas Indonesia', '2008', '5', 167, 52);
INSERT INTO `statistic_player` VALUES (209, 'Danang Syailendra Adhiyaksa Pratama Danang', 'Danang', 'danangsyailendra14@gmail.com', '81294642010', 'Depok', 'Jakarta Selatan', 'Power Forward', 'Small Forward', NULL, 'Universitas Indonesia', '2011', '23', 179, 98);
INSERT INTO `statistic_player` VALUES (210, 'Daniel Andiga Wibisana Danil', 'Danil', 'danielwibisana32@gmail.com', '82111743133', 'Jakarta Timur', 'Jakarta Timur', 'Small Forward', 'Power Forward', NULL, 'Universitas Indonesia', '2013', '2', 176, 70);
INSERT INTO `statistic_player` VALUES (211, 'Dariel arnaldo Aldo', 'Aldo', 'darielriel.mj@gmail.com', '81381063222', 'Bekasi', 'Jakarta pusat', 'Point Guard', 'Shooting Guard', NULL, 'Universitas Indonesia', '2013', '11', 168, 64);
INSERT INTO `statistic_player` VALUES (212, 'David sinaga David', 'David', 'davidpsinaga88@gmail.com', '81315884580', 'Cibubur', 'Jakarta selatan', 'Power Forward', 'Power Forward', NULL, 'Universitas Indonesia', '2006', '12', 175, 85);
INSERT INTO `statistic_player` VALUES (213, 'Dwika Herniawan Dwika', 'Dwika', 'dwikaherniawa@gmail.com', '81294081718', 'Jakarta Timur', 'Jakarta Selatan', 'Small Forward', 'Point Guard;Shooting', NULL, 'Universitas Indonesia', '2010', '12', 178, 69);
INSERT INTO `statistic_player` VALUES (214, 'Dzaki Adinda Husna Dzaki', 'Dzaki', 'dzakiadinda@gmail.com', '85610614995', 'Cinere', 'Jakarta Selatan Cinere', 'Power Forward', 'Shooting Guard;Small', NULL, 'Universitas Indonesia', '2014', '21', 183, 95);
INSERT INTO `statistic_player` VALUES (215, 'Ega Windratno Ega', 'Ega', 'siegaw@gmail.com', '81291776688', 'Jaksel', 'Jaksel', 'Center', 'Power Forward', NULL, 'Universitas Indonesia', '2006', '10', 184, 92);
INSERT INTO `statistic_player` VALUES (216, 'Fadhlan Al Abraar Fadhlan', 'Fadhlan', 'fadhlan.alabraar@gmail.com', '8121167171', 'Cipadu-Kreo Tangerang', 'Mampang', 'Center', 'Power Forward', NULL, 'Universitas Indonesia', '2007', '78', 184, 115);
INSERT INTO `statistic_player` VALUES (217, 'Fadhlan hamidi Midoy', 'Midoy', 'fadhlan.hamidi10@gmail.com', '81932228844', 'jakarta Selatan', 'Jakarta Selatan', 'Point Guard', 'Shooting Guard', NULL, 'Universitas Indonesia', '2011', '4', 173, 70);
INSERT INTO `statistic_player` VALUES (218, 'Farih Romdoni Putra Farih', 'Farih', 'farihromdoniputra@gmail.com', '8113640712', 'Jakarta Selatan', 'Jakarta Selatan', 'Shooting Guard', 'Point Guard', NULL, 'Universitas Indonesia', '2014', '13', 170, 85);
INSERT INTO `statistic_player` VALUES (219, 'Fhassi Maulavi Anfiqi Fhassi', 'Fhassi', 'fhassimaulavi@gmail.com', '81219173290', 'Cirendeu', 'SCBD', 'Shooting Guard', 'Point Guard', NULL, 'Universitas Indonesia', '2013', '6', 171, 65);
INSERT INTO `statistic_player` VALUES (220, 'Fredio Tarore Dio', 'Dio', 'fredioktavianus@gmail.com', '87781993327', 'Jakarta Timur', 'Jakarta Timur', 'Power Forward', 'Center', NULL, 'Universitas Indonesia', '2012', '10', 187, 88);
INSERT INTO `statistic_player` VALUES (221, 'Garry Ramadhany Garry', 'Garry', 'ramadhany.garry@gmail.com', '8568181109', 'Jakarta Utara', 'Jakarta Utara', 'Shooting Guard', 'Point Guard;Small Fo', NULL, 'Universitas Indonesia', '2009', '8', 172, 75);
INSERT INTO `statistic_player` VALUES (222, 'Gerry octaviano Gerry', 'Gerry', 'gerryoctaviano85@gmail.com', '82125919763', 'Jakarta selatan', 'Jakarta pusat', 'Point Guard', 'Shooting Guard', NULL, 'Universitas Indonesia', '2003', '9', 172, 83);
INSERT INTO `statistic_player` VALUES (223, 'Hendratanu Wijaya Hendra atau Tanu', 'Hendra atau Tanu', 'wijayahendratanu@gmail.com', '81230290960', 'Jakarta Selatan - Durentiga', 'Jakarta Selatan - Kuningan', 'Point Guard', 'Shooting Guard', NULL, 'Universitas Indonesia', '2008', '16', 170, 72);
INSERT INTO `statistic_player` VALUES (224, 'Henry Setiandi Golap', 'Golap', 'gowl_up2k@yahoo.com', '8129847948', 'Duren Sawit, Jaktim', 'Thamrin, Jakpus', 'Shooting Guard', 'Point Guard', NULL, 'Universitas Indonesia', '2000', '6', 170, 78);
INSERT INTO `statistic_player` VALUES (225, 'Hilmi aulia Hilmi', 'Hilmi', 'hilmibasketball@gmail.com', '8567577121', 'Jaktim', 'Jaktim', 'Shooting Guard', 'Power Forward', NULL, 'Universitas Indonesia', '2010', '10', 180, 85);
INSERT INTO `statistic_player` VALUES (226, 'husni mubarak husni', 'husni', 'husnimubarak93@gmail.com', '87883798730', 'jakarta timur', 'depok', 'Center', 'Shooting Guard;Power', NULL, 'Universitas Indonesia', '2010', '13', 177, 95);
INSERT INTO `statistic_player` VALUES (227, 'i gede aditya mahendra adit', 'adit', 'gedeaditya.89@gmail.com', '81283888265', 'bekasi', 'jakarta selatan', 'Shooting Guard', 'Point Guard', NULL, 'Universitas Indonesia', '2007', '8', 175, 75);
INSERT INTO `statistic_player` VALUES (228, 'Ibnu Azhar Ibnu/Ibs', 'Ibnu/Ibs', 'ibnu.azhar.suryadi@gmail.com', '82125214288', 'Jakarta Selatan', 'Jakarta Barat', 'Shooting Guard', 'Point Guard;Small Fo', NULL, 'Universitas Indonesia', '2011', '9', 180, 82);
INSERT INTO `statistic_player` VALUES (229, 'Ibnu fuad hasan Ibnu', 'Ibnu', 'inew03@gmail.com', '87884086700', 'Jakarta timur', 'Jakarta', 'Point Guard', 'Shooting Guard;Small', NULL, 'Universitas Katolik Atmajaya', '2008', '5', 178, 90);
INSERT INTO `statistic_player` VALUES (230, 'Ibrahim Fahmi Aziz Baim', 'Baim', 'ibralakazam@hotmail.com', '81284844717', 'Jakarta Selatan', 'Jakarta Barat', 'Power Forward', 'Small Forward;Center', NULL, 'Universitas Indonesia', '2015', '5', 188, 110);
INSERT INTO `statistic_player` VALUES (231, 'IGNASIUS FERDIE FERDIE', 'FERDIE', 'ignasiusferdie91@gmail.com', '8127054062', 'Jakarta Pusat', 'Jakarta Utara', 'Small Forward', 'Power Forward', NULL, 'Universitas Indonesia', '2009', '4', 176, 82);
INSERT INTO `statistic_player` VALUES (232, 'Imam Rachbini Imam', 'Imam', 'imam20297@gmail.com', '81283600241', 'Depok', '-', 'Small Forward', 'Shooting Guard;Power', NULL, 'Universitas Indonesia', '2015', '22', 175, 85);
INSERT INTO `statistic_player` VALUES (233, 'Indra P. Djokosoetono Indra', 'Indra', 'indra.priawan@me.com', '87884686452', 'Jakarta selatan', 'Jakarta selatan', 'Small Forward', 'Shooting Guard;Power', NULL, 'Universitas Indonesia', '2010', '9', 183, 80);
INSERT INTO `statistic_player` VALUES (234, 'Irfan Pradana Telling', 'Telling', 'pradana_irfan@hotmail.com', '87870095376', 'BSD', 'Jakarta selatan', 'Small Forward', 'Point Guard', NULL, 'Universitas Indonesia', '2007', '13', 180, 75);
INSERT INTO `statistic_player` VALUES (235, 'Josua Bonardo Marasi Jocu', 'Jocu', 'josuasitorus12@gmail.com', '81295701898', 'Jakarta Selatan ', 'Jakarta Selatan', 'Power Forward', 'Power Forward', NULL, 'Universitas Indonesia', '2003', '25', 178, 97);
INSERT INTO `statistic_player` VALUES (236, 'Justin Justin', 'Justin', 'justinsalim@hotmail.com', '81285252425', 'Jaksel', 'Jaksel', 'Small Forward', 'Power Forward', NULL, 'UW-madison', '2012', '0', 187, 82);
INSERT INTO `statistic_player` VALUES (237, 'M. Iqbal cakrabuana Cakra', 'Cakra', 'cakramicb@gmail.com', '82124656565', 'Jakarta pusat', '-', 'Center', 'Power Forward', NULL, 'Universitas Indonesia', '2014', '10', 190, 95);
INSERT INTO `statistic_player` VALUES (238, 'Muhamad Abrar Adrian  Abrar', 'Abrar', 'abrar.adrian@gmail.com', '81113003636', 'Jakarta Selatan', 'Jakarta Selatan', 'Center', 'Power Forward', NULL, 'Universitas Pelita Harapan', '2007', '7', 193, 92);
INSERT INTO `statistic_player` VALUES (239, 'Muhammad Arief Gucci Gucci', 'Gucci', 'ariefgucci@gmail.com', '6281319413351', 'Jakarta Timur', 'Jakarta Pusat', 'Shooting Guard', 'Small Forward', NULL, 'Universitas Indonesia', '2008', '9', 175, 77);
INSERT INTO `statistic_player` VALUES (240, 'Muhammad Harman Pradana  Harman', 'Harman', 'Pradana.reksodiputro@gmail.com', '81219198574', 'jakarta timur ', 'jakarta timur ', 'Small Forward', 'Shooting Guard', NULL, 'Universitas Indonesia', '2016', '16', 183, 87);
INSERT INTO `statistic_player` VALUES (241, 'Muhammad Idham Akbar Idham', 'Idham', 'midhamakbar@gmail.com', '81295332622', 'Jakarta selatan', 'Jakarta selatan', 'Small Forward', 'Point Guard;Shooting', NULL, 'Universitas Indonesia', '2007', '9', 182, 76);
INSERT INTO `statistic_player` VALUES (242, 'Muhammad Ilham Akbar Ilham', 'Ilham', 'muhammadilhamakbar@rocketmail.com', '81513610800', 'Jakarta Selatan', 'Jakarta Pusat', 'Shooting Guard', 'Small Forward;Power ', NULL, 'Universitas Padjadjaran', '2007', '11', 180, 75);
INSERT INTO `statistic_player` VALUES (243, 'Muhammad Ricky setiawan Ricky', 'Ricky', 'ricky.setiawan12th@gmail.com', '85718753302', 'Jakarta Selatan', 'Jakarta Pusat', 'Power Forward', 'Small Forward;Power ', NULL, 'Universitas Indonesia', '2010', '18', 183, 95);
INSERT INTO `statistic_player` VALUES (244, 'Muhammad Rizki Fajar Kiki', 'Kiki', 'rizkimuhammadfajar15@yahoo.com', '6281297289224', 'Depok', 'Karawang', 'Shooting Guard', 'Point Guard', NULL, 'Universitas Indonesia', '2014', '15', 174, 65);
INSERT INTO `statistic_player` VALUES (245, 'Muhammad Sabilal Rasyad Bilal', 'Bilal', 'muhammad.sabilal213@gmail.com', '85782191009', 'Jakarta Barat', 'Jakarta Selatan', 'Small Forward', 'Shooting Guard;Power', NULL, 'Universitas Indonesia', '2014', '25', 178, 88);
INSERT INTO `statistic_player` VALUES (246, 'Muhammad Triadi Kurnia Hadi Triadi', 'Triadi', 'muhammad.triadi081@gmail.com', '81286822169', 'Jakarta Barat', 'Jakarta Selatan', 'Shooting Guard', 'Point Guard', NULL, 'Universitas Pelita Harapan', '2010', '6', 175, 80);
INSERT INTO `statistic_player` VALUES (247, 'Muhammad Yusuf Adriansyah  Adri', 'Adri', 'm.y.adriansyah@gmail.com', '81218294060', 'Jakarta selatan', 'Kuala Lumpur', 'Point Guard', 'Shooting Guard;Small', NULL, 'Universitas Indonesia', '2013', '11', 180, 69);
INSERT INTO `statistic_player` VALUES (248, 'Mulia Angara Angga', 'Angga', 'mulia.angara@gmail.com', '8115552772', 'Jakarta Selatan', 'Jakarta Pusat', 'Power Forward', 'Center', NULL, 'Universitas Indonesia', '2011', '7', 192, 90);
INSERT INTO `statistic_player` VALUES (249, 'Naufal Fauzan Pratikto Cire', 'Cire', 'naufalfauzan19@gmail.com', '81289698987', 'Depok', 'Jakarta Selatan', 'Point Guard', 'Shooting Guard', NULL, 'Universitas Indonesia', '2015', '6', 168, 60);
INSERT INTO `statistic_player` VALUES (250, 'odi nan sati hasiholan Odi', 'Odi', 'odihasiholan@gmail.com', '8119412828', 'Jakarta timur', 'Bekasi selatan, galaxy', 'Small Forward', 'Power Forward;Center', NULL, 'The national university of Malaysia', '2010', '0', 178, 78);
INSERT INTO `statistic_player` VALUES (251, 'Paulus Pirton Paulus', 'Paulus', 'pauluspirton@gmail.com', '82213000134', 'Cibubur', 'Jakarta Selatan', 'Power Forward', 'Small Forward', NULL, 'Universitas Indonesia', '2012', '8', 180, 82);
INSERT INTO `statistic_player` VALUES (252, 'Randy Adianto Prathama Randy', 'Randy', 'randyadianto24@gmail.com', '85856697584', 'Kemang, jakarta selatan', 'Jakarta selatan', 'Small Forward', 'Shooting Guard;Power', NULL, 'Universitas Indonesia', '2014', '15', 175, 82);
INSERT INTO `statistic_player` VALUES (253, 'Richie Gradiyanto Molenaar Richie', 'Richie', 'richiemolenaar21@gmail.com', '81315001791', 'Tangerang Selatan', 'Jakarta Selatan', 'Point Guard', 'Small Forward', NULL, 'Perbanas', '2007', '13', 173, 89);
INSERT INTO `statistic_player` VALUES (254, 'Rizka Wirasatya Wira', 'Wira', 'wirasats@gmail.com', '82113938011', 'Jakarta Timur', 'Jakarta Selatan', 'Point Guard', 'Shooting Guard;Small', NULL, 'Universitas Indonesia', '2012', '13', 175, 78);
INSERT INTO `statistic_player` VALUES (255, 'Rizki Dwianda Rildo Dewe', 'Dewe', 'rizkidwianda@gmail.com', '81212141419', 'Jakarta Selatan', 'Jakarta Pusat', 'Shooting Guard', 'Shooting Guard;Small', NULL, 'Universitas Indonesia', '2007', '12', 178, 75);
INSERT INTO `statistic_player` VALUES (256, 'tamma anjara Tamma', 'Tamma', 'tammaanjara@yahoo.com', '81290931148', 'Jakarta Barat', 'Jakarta Selatan', 'Point Guard', 'Shooting Guard;Small', NULL, 'Edith Cowan University ', '2008', '8', 180, 82);
INSERT INTO `statistic_player` VALUES (257, 'Yano Andriyanto Yano', 'Yano, Onay', 'yanoandri@gmail.com', '6285658971569', 'Depok', 'Jakarta Selatan', 'Point Guard', 'Shooting Guard;Small', NULL, 'Universitas Indonesia', '2015', '34', 171, 70);
INSERT INTO `statistic_player` VALUES (258, 'Yusuf Fauzi', 'Fauzi', 'yusuf.fauzi87@gmail.com', '81237280976', 'Palembang', 'Sumatera Selatan', 'Point Guard', 'Shooting Guard', NULL, 'Universitas Indonesia', '2009', '17', 168, 60);

-- ----------------------------
-- Table structure for statistic_playerattendance
-- ----------------------------
DROP TABLE IF EXISTS `statistic_playerattendance`;
CREATE TABLE `statistic_playerattendance`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_attendance` date NULL DEFAULT NULL,
  `player_id` int(11) NOT NULL,
  `attendance_time` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `remarks` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `statistic_playeratte_player_id_106429d6_fk_statistic`(`player_id`) USING BTREE,
  CONSTRAINT `statistic_playeratte_player_id_106429d6_fk_statistic` FOREIGN KEY (`player_id`) REFERENCES `statistic_player` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for statistic_team
-- ----------------------------
DROP TABLE IF EXISTS `statistic_team`;
CREATE TABLE `statistic_team`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_name` varchar(120) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `team_formed_date` date NULL DEFAULT NULL,
  `play_count` int(11) NOT NULL,
  `win_count` int(11) NOT NULL,
  `draw_count` int(11) NOT NULL,
  `lose_count` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for statistic_teammember
-- ----------------------------
DROP TABLE IF EXISTS `statistic_teammember`;
CREATE TABLE `statistic_teammember`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_attendance_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `statistic_teammember_player_attendance_id_543305d3_fk_statistic`(`player_attendance_id`) USING BTREE,
  INDEX `statistic_teammember_team_id_6a4ccda7_fk_statistic_team_id`(`team_id`) USING BTREE,
  CONSTRAINT `statistic_teammember_player_attendance_id_543305d3_fk_statistic` FOREIGN KEY (`player_attendance_id`) REFERENCES `statistic_playerattendance` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `statistic_teammember_team_id_6a4ccda7_fk_statistic_team_id` FOREIGN KEY (`team_id`) REFERENCES `statistic_team` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for statistic_match
-- ----------------------------
DROP TABLE IF EXISTS `statistic_match`;
CREATE TABLE `statistic_match`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `home_score` int(11) NOT NULL,
  `away_score` int(11) NOT NULL,
  `queue` int(11) NULL DEFAULT NULL,
  `schedule_date` date NULL DEFAULT NULL,
  `is_finished` tinyint(1) NOT NULL,
  `away_team_id` int(11) NOT NULL,
  `home_team_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `statistic_match_away_team_id_89c6a856_fk_statistic_team_id`(`away_team_id`) USING BTREE,
  INDEX `statistic_match_home_team_id_61a5c5c5_fk_statistic_team_id`(`home_team_id`) USING BTREE,
  CONSTRAINT `statistic_match_away_team_id_89c6a856_fk_statistic_team_id` FOREIGN KEY (`away_team_id`) REFERENCES `statistic_team` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `statistic_match_home_team_id_61a5c5c5_fk_statistic_team_id` FOREIGN KEY (`home_team_id`) REFERENCES `statistic_team` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;



-- ----------------------------
-- Table structure for statistic_stats
-- ----------------------------
DROP TABLE IF EXISTS `statistic_stats`;
CREATE TABLE `statistic_stats`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `score` int(11) NULL DEFAULT NULL,
  `score_attempt` int(11) NULL DEFAULT NULL,
  `three_point_score` int(11) NULL DEFAULT NULL,
  `three_point_attempt_score` int(11) NULL DEFAULT NULL,
  `assists` int(11) NULL DEFAULT NULL,
  `steal` int(11) NULL DEFAULT NULL,
  `block` int(11) NULL DEFAULT NULL,
  `turnover` int(11) NULL DEFAULT NULL,
  `offensive_rebound` int(11) NULL DEFAULT NULL,
  `defensive_rebound` int(11) NULL DEFAULT NULL,
  `match_id` int(11) NOT NULL,
  `player_attendance_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `statistic_stats_match_id_0b330eda_fk_statistic_match_id`(`match_id`) USING BTREE,
  INDEX `statistic_stats_player_attendance_id_8259a9b7_fk_statistic`(`player_attendance_id`) USING BTREE,
  INDEX `statistic_stats_team_id_cdb6a0e0_fk_statistic_team_id`(`team_id`) USING BTREE,
  CONSTRAINT `statistic_stats_match_id_0b330eda_fk_statistic_match_id` FOREIGN KEY (`match_id`) REFERENCES `statistic_match` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `statistic_stats_player_attendance_id_8259a9b7_fk_statistic` FOREIGN KEY (`player_attendance_id`) REFERENCES `statistic_playerattendance` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `statistic_stats_team_id_cdb6a0e0_fk_statistic_team_id` FOREIGN KEY (`team_id`) REFERENCES `statistic_team` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;



SET FOREIGN_KEY_CHECKS = 1;
