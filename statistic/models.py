from django.db import models
import json

# Create your models here.


class Player(models.Model):
    full_name = models.CharField(max_length=120, blank=True, null=True)
    nick_name = models.CharField(max_length=120, blank=True, null=True)
    email = models.EmailField(max_length=100, blank=True, null=True)
    phone_number = models.CharField(max_length=40, blank=True, null=True)
    home_address = models.TextField(blank=True, null=True)
    work_address = models.TextField(blank=True, null=True)
    position = models.CharField(max_length=20, blank=True, null=True)
    alt_position = models.CharField(max_length=20, blank=True, null=True)
    birth_date = models.DateField(blank=True, null=True)
    from_universities = models.CharField(max_length=120, blank=True, null=True)
    class_year = models.CharField(max_length=4, blank=True, null=True)
    back_number = models.CharField(max_length=100, blank=True, null=True)
    height_in_centimeter = models.IntegerField(blank=True, null=True)
    weight_in_kilogram = models.IntegerField(blank=True, null=True)
    overall_rating = models.CharField(max_length=100, blank=True, null=True)
    
    def __repr__(self):
        return f"id: {self.id}, fullname : {self.full_name}, nickname: {self.nick_name}, position: {self.position}, alt_position: {self.alt_position}, height: {self.height_in_centimeter}, weight: {self.weight_in_kilogram}"


class PlayerAttendance(models.Model):
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    date_attendance = models.DateField(blank=True, null=True)
    attendance_time = models.CharField(max_length=5, blank=True, null=True)
    remarks = models.CharField(max_length=100, blank=True, null=True)


class Team(models.Model):
    team_name = models.CharField(max_length=120)
    team_formed_date = models.DateField(blank=True, null=True)
    play_count = models.IntegerField(default=0)
    win_count = models.IntegerField(default=0)
    draw_count = models.IntegerField(default=0)
    lose_count = models.IntegerField(default=0)

    def count_player(self):
        member = TeamMember.objects.filter(team_id__exact=self.id)
        return len(member)


class TeamMember(models.Model):
    player_attendance = models.ForeignKey(
        PlayerAttendance, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)


class Match(models.Model):
    home_team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='%(class)s_home_team')
    away_team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='%(class)s_away_team')
    home_score = models.IntegerField(default=0)
    away_score = models.IntegerField(default=0)
    queue = models.IntegerField(blank=True, null=True)
    schedule_date = models.DateField(blank=True, null=True)
    is_finished = models.BooleanField(default=False)
    
    def game_results_score(self):
        results = f'{self.home_score} vs {self.away_score}'
        return results


class Stats(models.Model):
    player_attendance = models.ForeignKey(
        PlayerAttendance, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    match = models.ForeignKey(Match, on_delete=models.CASCADE)
    score = models.IntegerField(blank=True, null=True)
    score_attempt = models.IntegerField(blank=True, null=True)
    three_point_score = models.IntegerField(blank=True, null=True)
    three_point_attempt_score = models.IntegerField(blank=True, null=True)
    assists = models.IntegerField(blank=True, null=True)
    steal = models.IntegerField(blank=True, null=True)
    block = models.IntegerField(blank=True, null=True)
    turnover = models.IntegerField(blank=True, null=True)
    offensive_rebound = models.IntegerField(blank=True, null=True)
    defensive_rebound = models.IntegerField(blank=True, null=True)
    freethrow_score = models.IntegerField(blank=True, null=True)
    freethrow_attempt = models.IntegerField(blank=True, null=True)
    foul = models.IntegerField(blank=True, null=True)



