class ResponseInfo:
    @staticmethod
    def get_member_info_as_json(member, team):
        data = {
            'member_id' : member.id,
            'attendance_id' : member.player_attendance.id,
            'player': {
                'id': member.player_attendance.player.id,
                'full_name': member.player_attendance.player.full_name,
                'position': member.player_attendance.player.position,
                'alt_position': member.player_attendance.player.alt_position,
                'height_in_centimeter': member.player_attendance.player.height_in_centimeter,
                'weight_in_kilogram': member.player_attendance.player.weight_in_kilogram,
                'overall_rating': member.player_attendance.player.overall_rating
            },
            'team': {
                'id': team.id,
                'name': team.team_name
            }
        }
        return data
    
    @staticmethod
    def stats_info_as_json(id, value, tipe):
        data = {
            'id' : id,
            'value' : value,
            'type' : tipe
        }
        return data