class Parameter:
    # Flag home and away
    TEAM_HOME_FLAG = 1
    TEAM_AWAY_FLAG = 0
    # Flag stats parameters
    SCORE = 1
    SCORE_ATTEMPT = 2
    THREE_POINT_SCORE = 3
    THREE_POINT_SCORE_ATTEMPT = 4
    ASSISTS = 5
    STEAL = 6
    BLOCK = 7
    TURNOVER = 8
    OFFENSIVE_REBOUND = 9
    DEFENSIVE_REBOUND = 10
    FREETHROW_SCORE = 11
    FREETHROW_ATTEMPT = 12
    FOUL = 13
