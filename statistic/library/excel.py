from openpyxl import load_workbook
import xlrd

class AttendanceExcel:
    def __init__(self, uploaded_file):
        self.uploaded_file = uploaded_file

    def read_excel_to_dictionary(self):
        extension = self.uploaded_file.name[len(self.uploaded_file.name) - 4:len(self.uploaded_file.name)]
        workbook = None
        records = []
        count = 0
        if extension == '.xls':
            content = self.uploaded_file.read()
            workbook = xlrd.open_workbook(file_contents=content)
            worksheet = workbook.sheet_by_index(0)
            for row in range(0, worksheet.nrows):    
                if count == 0 or worksheet.cell(row, 1).value == '' or worksheet.cell(row, 1).value == None:
                        count += 1
                        continue

                temp = {
                    'email': worksheet.cell(row, 3).value, 
                    'full_name' : worksheet.cell(row, 1).value,
                    'nick_name' : worksheet.cell(row, 2).value,
                    'position' : worksheet.cell(row, 5).value,
                    'alt_position' : worksheet.cell(row, 6).value,
                    'attendance_time': worksheet.cell(row, 7).value,
                    'remarks' : worksheet.cell(row, 8).value,
                    'back_number' : int(worksheet.cell(row, 9).value),
                    'overall_rating' : "{:.2f}".format(worksheet.cell(row, 10).value),
                }
                records.append(temp)
                count += 1
            # print(worksheet.cell(0, 0).value)
        else:
            workbook = load_workbook(self.uploaded_file)
            sheet = workbook.active
            for row in sheet.values:
                if count == 0 or row[1] == '' or row[1] == None:
                    count += 1
                    continue

                temp = {
                    'email': row[3], 
                    'full_name' : row[1],
                    'nick_name' : row[2],
                    'position' : row[5],
                    'alt_position' : row[6],
                    'attendance_time': row[7],
                    'remarks' : row[8],
                    'back_number': int(row[9]),
                    'overall_rating': "{:.2f}".format(row[10])
                }
                records.append(temp)
                count += 1
        return records

class RegisterExcel:
    def __init__(self, uploaded_file):
        self.uploaded_file = uploaded_file
    
    def read_excel_to_dictionary(self):
        extension = self.uploaded_file.name[len(self.uploaded_file.name) - 5:len(self.uploaded_file.name)]
        workbook = None
        records = []
        count = 0
        if extension == '.xlsx':
            workbook = load_workbook(self.uploaded_file)
            sheet = workbook.active
            for row in sheet.values:
                if count == 0 or row[1] == '' or row[1] == None:
                    count += 1
                    continue

                temp = {
                    'email': row[1], 
                    'full_name': row[2],
                    'nick_name': row[3],
                    'phone': row[4],
                    'from_universities' : row[5],
                    'class_year' : row[6],
                    'back_number' : row[8],
                    'height' : row[9],
                    'weight': row[10],
                    'position' : row[11],
                    'alt_position' : row[12],
                    'work_address' : row[13],
                    'home_address' : row[14],
                    'overall_rating': "{:.2f}".format(row[15])
                }
                records.append(temp)
                count += 1
        else:
            content = self.uploaded_file.read()
            workbook = xlrd.open_workbook(file_contents=content)
            worksheet = workbook.sheet_by_index(0)
            for row in range(0, worksheet.nrows):    
                if count == 0 or worksheet.cell(row, 1).value == '' or worksheet.cell(row, 1).value == None:
                        count += 1
                        continue
                temp = {
                    'email': worksheet.cell(row, 1).value, 
                    'full_name' : worksheet.cell(row, 2).value,
                    'nick_name' : worksheet.cell(row, 3).value,
                    'phone': worksheet.cell(row, 4).value,
                    'from_universities' : worksheet.cell(row, 5).value,
                    'class_year' : worksheet.cell(row, 6).value,
                    'back_number' : worksheet.cell(row, 8).value,
                    'height' : worksheet.cell(row, 9).value,
                    'weight': worksheet.cell(row, 10).value,
                    'position' : worksheet.cell(row, 11).value,
                    'alt_position' : worksheet.cell(row, 12).value,
                    'work_address' : worksheet.cell(row, 13).value,
                    'home_address' : worksheet.cell(row, 14).value,
                    'overall_rating' : "{:.2f}".format(worksheet.cell(row, 15).value)
                }
                records.append(temp)
                count += 1
        return records
    

