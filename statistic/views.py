from django.shortcuts import render, redirect
from django.urls import reverse
from django.http import HttpResponse, JsonResponse
from django.db.models import Q
from datetime import datetime
from statistic.models import Team, TeamMember, PlayerAttendance, Player, Match, Stats
from statistic.datasource.source import Source
from statistic.responsemodel.response import ResponseInfo
from statistic.library.helpers import Parameter
from statistic.library.excel import AttendanceExcel, RegisterExcel
from random import choice
from decimal import Decimal

today = datetime.now().strftime('%B %d, %Y')
today = datetime.strptime(today,'%B %d, %Y')

# Create your views here.
def index(request):
    attendance_list = PlayerAttendance.objects.filter(date_attendance__exact=today)
    return render(request, 'dashboard/index.html', {'attendance_list' : attendance_list, 'today' : today})

def cancel_attendance(request, id):
    attendance = PlayerAttendance.objects.get(pk=id)
    attendance.delete()
    # call for redirect
    base = reverse('dashboard')
    url_redirect = f'{base}'
    return redirect(url_redirect)
    

def bulk_cancel_attendance(request):
    if request.method == 'POST':
        if request.POST.getlist('check-cancel'):
            attendance = PlayerAttendance.objects.filter(pk__in=request.POST.getlist('check-cancel')).filter(date_attendance__exact=today)
            attendance.delete()
    # call for redirect
    base = reverse('dashboard')
    url_redirect = f'{base}'
    return redirect(url_redirect)

def list_player(request):
    player_list = Player.objects.all()
    return render(request, 'dashboard/list_player.html', {'player_list' : player_list})

def edit_player(request, id):
    player = Player.objects.get(pk=id)
    print(player)
    if request.method == 'POST':
        player.full_name = request.POST.get('player_full_name')
        player.nick_name = request.POST.get('player_nick_name')
        player.back_number = request.POST.get('player_back_number')
        player.overall_rating = request.POST.get('player_overall_rating')
        player.position = request.POST.get('player_position')
        player.alt_position = request.POST.get('player_alt_position')
        player.save()
        # call for redirect
        base = reverse('dashboard.player.list')
        url_redirect = f'{base}'
        return redirect(url_redirect)

    return render(request, 'dashboard/edit_player.html', {'player': player})

def upload_player(request):
    today_date = datetime.now().strftime('%Y-%m-%d')
    if request.method == 'POST':
        uploaded_file = request.FILES['file_excel_player']
        excel = RegisterExcel(uploaded_file)
        list_attendance = excel.read_excel_to_dictionary()
        for player in list_attendance:
            existing_player = Player.objects.filter(email__exact=player['email'])
            if not existing_player:
                new_player = Player()
                new_player.email = player['email']
                new_player.full_name = player['full_name']
                new_player.nick_name = player['nick_name']
                new_player.phone_number = player['phone']
                new_player.from_universities = player['from_universities']
                new_player.class_year = player['class_year']
                new_player.back_number = player['back_number']
                new_player.height_in_centimeter = int(player['height'])
                new_player.weight_in_kilogram = int(player['weight'])
                new_player.position = player['position']
                new_player.alt_position = player['alt_position']
                new_player.home_address = player['home_address']
                new_player.work_address = player['work_address']
                new_player.save()
            else:
                existing_player.full_name = player['full_name']
                existing_player.nick_name = player['nick_name']
                existing_player.phone_number = player['phone']
                existing_player.from_universities = player['from_universities']
                existing_player.class_year = player['class_year']
                existing_player.back_number = player['back_number']
                existing_player.height_in_centimeter = int(player['height'])
                existing_player.weight_in_kilogram = int(player['weight'])
                existing_player.position = player['position']
                existing_player.alt_position = player['alt_position']
                existing_player.home_address = player['home_address']
                existing_player.work_address = player['work_address']
                existing_player.save()
             # call for redirect
        base = reverse('dashboard')
        url_redirect = f'{base}'
        return redirect(url_redirect)
    
    return render(request, 'dashboard/upload_player.html', {})


def upload_attendance(request):
    today_date = datetime.now().strftime('%Y-%m-%d')
    if request.method == 'POST':
        uploaded_file = request.FILES['file_excel_attendance']
        excel = AttendanceExcel(uploaded_file)
        list_attendance = excel.read_excel_to_dictionary()
        for player in list_attendance:
            attendance_today = PlayerAttendance()
            attendance_today.date_attendance = today_date
            temp = Player.objects.filter(email__exact=player['email'])
            if temp:
                player_id = temp[0].id
                existing_player = Player.objects.get(pk=player_id)
                if existing_player:
                    attendance_player_existing = PlayerAttendance.objects.filter(
                        Q(player_id=existing_player.id),
                        Q(date_attendance=today_date)
                    )
                    if not attendance_player_existing:
                        attendance_today.player = existing_player
                        attendance_today.remarks = player['remarks']
                        attendance_today.attendance_time = player['attendance_time']
                        attendance_today.save()
            else:
                fresh_member = Player()
                fresh_member.email = player['email']
                fresh_member.full_name = player['full_name']
                fresh_member.nick_name = player['nick_name']
                fresh_member.position = player['position']
                fresh_member.alt_position = player['alt_position']
                fresh_member.back_number = player['back_number']
                fresh_member.overall_rating = player['overall_rating']
                fresh_member.save()
                attendance_today.player = fresh_member
                attendance_today.remarks = player['remarks']
                attendance_today.attendance_time = player['attendance_time']
                attendance_today.save()

            
             # call for redirect
        base = reverse('dashboard')
        url_redirect = f'{base}'
        return redirect(url_redirect)

    return render(request, 'dashboard/upload_attendance.html', {})


def create_team(request):
    if request.method == 'POST':
        count = int(request.POST.get("team_count")) + 1
        for x in range(1, count):
            new_team = Team()
            new_team.team_name = f'Team {x}'
            new_team.team_formed_date = datetime.now().strftime('%Y-%m-%d')
            new_team.save()

        team_list = Team.objects.filter(
            team_formed_date__exact=new_team.team_formed_date)
        return render(request, 'dashboard/list_team.html', {'team_list': team_list})
    return render(request, 'dashboard/create_team.html', {})

def edit_team(request, id):
    if request.method == 'POST':
        team_new_name = request.POST.get("team_name")
        team = Team.objects.get(pk=id)
        team.team_name = team_new_name
        team.save()

        team_list = Team.objects.filter(
            team_formed_date__exact=team.team_formed_date)
        return render(request, 'dashboard/list_team.html', {'team_list': team_list})

    team = Team.objects.get(pk=id)
    return render(request, 'dashboard/edit_team.html', {'team': team})
        


def list_team(request):
    today_date = datetime.now().strftime('%Y-%m-%d')
    team_list = Team.objects.filter(team_formed_date__exact=today_date)
    context = {'team_list': team_list}
    return render(request, 'dashboard/list_team.html', context)


def team_detail(request, id):
    team_info = Team.objects.get(pk=id)
    team_member = Source.get_all_team_member(id)
    player_who_attend = Source.get_list_of_attendance()
    return render(request, 'dashboard/detail_member.html', {'team_member': team_member, 'player_list': player_who_attend, 'team_info': team_info})


def random_pick(request, id):
    today = datetime.now().strftime('%Y-%m-%d')
    team_member = TeamMember.objects.filter(team_id__exact=id)
    if team_member:
        team_member.delete()
    team = Team.objects.get(pk=id)
    for x in range(5):
        is_available = True
        player_attend = Source.get_list_of_attendance()
        someone = choice(player_attend)
        attendance = PlayerAttendance.objects.get(
            pk=someone.player_attendance_id)
        if is_available:
            member = TeamMember()
            member.player_attendance = attendance
            member.team = team
            member.save()

    # call for redirect
    base = reverse('dashboard')
    url_redirect = f'team/detail/{id}'
    url_redirect = f'{base}{url_redirect}'
    return redirect(url_redirect)


def list_match(request):
    today = datetime.now().strftime('%Y-%m-%d')
    if request.method == 'POST':
        match_list = Match.objects.filter(schedule_date__exact=today)
        home_id = int(request.POST.get("home_team"))
        away_id = int(request.POST.get("away_team"))
        home_team = Team.objects.get(pk=home_id)
        away_team = Team.objects.get(pk=away_id)
        match = Match()
        match.home_team = home_team
        match.away_team = away_team
        match.schedule_date = today
        match.queue = len(match_list) + 1
        match.save()

    match_list = Source.get_list_of_total_stats(today)
    team_list = Team.objects.filter(team_formed_date__exact=today)
    ongoing_matches = Source.get_list_of_total_stats(today)
    matches = Match.objects.filter(schedule_date__exact=today, is_finished__exact=0)

    stats_list = []
    count = len(list(ongoing_matches))
    is_even = len(stats_list) % 2 == 0

    for i in range(0, count, 2):
        if i == count - 1:
            break
        
        match_id = ongoing_matches[i].id
        ongoing = {
            'home': 
                {
                    'name': ongoing_matches[i].team, 
                    'score': int(ongoing_matches[i].score), 
                    'members': Source.get_stats_of_member(match_id, ongoing_matches[i].team_id)
                }, 
            'away': 
                {
                    'name': ongoing_matches[i + 1].team, 
                    'score': int(ongoing_matches[i + 1].score),
                    'members': Source.get_stats_of_member(match_id, ongoing_matches[i + 1].team_id)
                }
        }
        stats_list.append(ongoing)

    return render(request, 'dashboard/list_match.html', {'team_list': team_list, 'match_list': matches, 'stats_list': stats_list})


def match_pick_team(request, id):
    match = Match.objects.get(pk=id)
    return render(request, 'dashboard/match_pick_team.html', {'match': match})


def match_pick_stats(request, id, flag_team):
    match = Match.objects.get(pk=id)
    team_id = None
    team = None
    if flag_team == Parameter.TEAM_HOME_FLAG:
        stats_list = Stats.objects.filter(
            Q(match_id=match.id),
            Q(team_id=match.home_team.id)
        )
        team_id = match.home_team.id
        team = match.home_team
    else:
        stats_list = Stats.objects.filter(
            Q(match_id=match.id),
            Q(team_id=match.away_team.id)
        )
        team_id = match.away_team.id
        team = match.away_team

    if len(stats_list) == 0:
        stats_list = []
        team_member = TeamMember.objects.filter(
            Q(team_id__exact=team_id),
        )
        for person in team_member:
            stats = Stats()
            stats.match = match
            stats.player_attendance = person.player_attendance
            stats.team = person.team
            stats.score = 0
            stats.score_attempt = 0
            stats.three_point_score = 0
            stats.three_point_attempt_score = 0
            stats.assists = 0
            stats.steal = 0
            stats.block = 0
            stats.turnover = 0
            stats.offensive_rebound = 0
            stats.defensive_rebound = 0
            stats.freethrow_score = 0
            stats.freethrow_attempt = 0
            stats.foul = 0
            stats.save()
            stats_list.append(stats)

    return render(request, 'dashboard/match_pick_stats.html', {'stats_list' : stats_list, 'match': match,'team' : team,'flag': flag_team})

def finish_game(request, id, team_id, flag):
    match = Match.objects.get(pk=id)
    team = None
    result = None
    #itung masing masing skor tiap tim
    #jika match belum selesai
    if not match.is_finished:
        team = Team.objects.get(pk=team_id)
        team.play_count += 1
        #hitung stats dari tim home
        stat_list = Stats.objects.filter(
            match_id__exact=id,
            team_id__exact=team_id
        )
        two_point_score = sum([stat.score for stat in stat_list]) * 2
        three_point_score = sum([stat.three_point_score for stat in stat_list]) * 3
        freethrow_score = sum([stat.freethrow_score for stat in stat_list])
        result = two_point_score + three_point_score + freethrow_score
    #itung statistik tiap tim (home dan away) secara spesifik
    #jika match udah berjalan dan ini dari tim home
    if flag == Parameter.TEAM_HOME_FLAG:
        match.home_score = result
        #kalo score away udah di record sebelumnya
        if match.away_score > 0:
            if match.home_score > match.away_score:
                team.win_count += 1
                match.away_team.lose_count += 1
            #kalo home kalah
            elif match.home_score < match.away_score:
                team.lose_count += 1
                match.away_team.win_count += 1
            #selain itu dia pasti seri
            else:
                team.draw_count += 1
                match.away_team.draw_count += 1
            match.away_team.save()
    #kalo dia away
    else:
        match.away_score = result
        #kalo score home udah di record sebelumnya
        if match.home_score > 0:
            if match.away_score > match.home_score:
                team.win_count += 1
                match.home_team.lose_count += 1
            #kalo home kalah
            elif match.away_score < match.home_score:
                team.lose_count += 1
                match.home_team.win_count += 1
            #selain itu dia pasti seri
            else:
                team.draw_count += 1
                match.home_team.draw_count += 1
            match.home_team.save()
    
    team.save()
    
    #kalau kedua tim skornya udah lebih dari 0
    if match.home_score > 0 and match.away_score > 0:
        #setelah mencatat ubah flag is finished menjadi true
        match.is_finished = True
    
    match.save()
    # call for redirect
    base = reverse('dashboard')
    url_redirect = f'match/list/'
    url_redirect = f'{base}{url_redirect}'
    return redirect(url_redirect)


def list_setting(request):
    return render(request, 'dashboard/list_settings.html', {})


def create_setting(request):
    return render(request, 'dashboard/create_setting.html', {})

# function method
def pick_player(request, attendance_id, team_id):
    team = Team.objects.get(pk=team_id)
    attendance = PlayerAttendance.objects.get(pk=attendance_id)
    new_member = TeamMember()
    new_member.player_attendance = attendance
    new_member.team = team
    new_member.save()
    data = ResponseInfo.get_member_info_as_json(new_member, team)
    return JsonResponse(data)


def drop_player(request, id):
    member = TeamMember.objects.get(pk=id)
    data = ResponseInfo.get_member_info_as_json(member, member.team)
    deleted = member.delete()
    return JsonResponse(data)

def increase_stats(request, id, stats_id):
    stats = Stats.objects.get(pk=id)
    value = None
    tipe = None
    if stats_id == Parameter.SCORE:
        stats.score += 1
        value = stats.score
        tipe = Parameter.SCORE
    elif stats_id == Parameter.SCORE_ATTEMPT:
        stats.score_attempt += 1
        value = stats.score_attempt
        tipe = Parameter.SCORE_ATTEMPT
    elif stats_id == Parameter.THREE_POINT_SCORE:
        stats.three_point_score += 1
        value = stats.three_point_score
        tipe = Parameter.THREE_POINT_SCORE
    elif stats_id == Parameter.THREE_POINT_SCORE_ATTEMPT:
        stats.three_point_attempt_score += 1
        value = stats.three_point_attempt_score
        tipe = Parameter.THREE_POINT_SCORE_ATTEMPT
    elif stats_id == Parameter.ASSISTS:
        stats.assists += 1
        value = stats.assists
        tipe = Parameter.ASSISTS
    elif stats_id == Parameter.STEAL:
        stats.steal += 1
        value = stats.steal
        tipe = Parameter.STEAL
    elif stats_id == Parameter.BLOCK:
        stats.block += 1
        value = stats.block
        tipe = Parameter.BLOCK
    elif stats_id == Parameter.TURNOVER:
        stats.turnover += 1
        value = stats.turnover
        tipe = Parameter.TURNOVER
    elif stats_id == Parameter.OFFENSIVE_REBOUND:
        stats.offensive_rebound += 1
        value = stats.offensive_rebound
        tipe = Parameter.OFFENSIVE_REBOUND
    elif stats_id == Parameter.DEFENSIVE_REBOUND:
        stats.defensive_rebound += 1
        value = stats.defensive_rebound
        tipe = Parameter.DEFENSIVE_REBOUND
    elif stats_id == Parameter.FREETHROW_SCORE:
        stats.freethrow_score += 1
        value = stats.freethrow_score
        tipe = Parameter.FREETHROW_SCORE
    elif stats_id == Parameter.FREETHROW_ATTEMPT:
        stats.freethrow_attempt += 1
        value = stats.freethrow_attempt
        tipe = Parameter.FREETHROW_ATTEMPT
    elif stats_id == Parameter.FOUL:
        stats.foul += 1
        value = stats.foul
        tipe = Parameter.FOUL
    stats.save()
    data = ResponseInfo.stats_info_as_json(id, value, tipe)
    return JsonResponse(data)

def decrease_stats(request, id, stats_id):
    stats = Stats.objects.get(pk=id)
    value = None
    tipe = None
    if stats_id == Parameter.SCORE:
        stats.score -= 1
        value = stats.score
        tipe = Parameter.SCORE
    elif stats_id == Parameter.SCORE_ATTEMPT:
        stats.score_attempt -= 1
        value = stats.score_attempt
        tipe = Parameter.SCORE_ATTEMPT
    elif stats_id == Parameter.THREE_POINT_SCORE:
        stats.three_point_score -= 1
        value = stats.three_point_score
        tipe = Parameter.THREE_POINT_SCORE
    elif stats_id == Parameter.THREE_POINT_SCORE_ATTEMPT:
        stats.three_point_attempt_score -= 1
        value = stats.three_point_attempt_score
        tipe = Parameter.THREE_POINT_SCORE_ATTEMPT
    elif stats_id == Parameter.ASSISTS:
        stats.assists -= 1
        value = stats.assists
        tipe = Parameter.ASSISTS
    elif stats_id == Parameter.STEAL:
        stats.steal -= 1
        value = stats.steal
        tipe = Parameter.STEAL
    elif stats_id == Parameter.BLOCK:
        stats.block -= 1
        value = stats.block
        tipe = Parameter.BLOCK
    elif stats_id == Parameter.TURNOVER:
        stats.turnover -= 1
        value = stats.turnover
        tipe = Parameter.TURNOVER
    elif stats_id == Parameter.OFFENSIVE_REBOUND:
        stats.offensive_rebound -= 1
        value = stats.offensive_rebound
        tipe = Parameter.OFFENSIVE_REBOUND
    elif stats_id == Parameter.DEFENSIVE_REBOUND:
        stats.defensive_rebound -= 1
        value = stats.defensive_rebound
        tipe = Parameter.DEFENSIVE_REBOUND
    elif stats_id == Parameter.FREETHROW_SCORE:
        stats.freethrow_score -= 1
        value = stats.freethrow_score
        tipe = Parameter.FREETHROW_SCORE
    elif stats_id == Parameter.FREETHROW_ATTEMPT:
        stats.freethrow_attempt -= 1
        value = stats.freethrow_attempt
        tipe = Parameter.FREETHROW_ATTEMPT
    elif stats_id == Parameter.FOUL:
        stats.foul -= 1
        value = stats.foul
        tipe = Parameter.FOUL
    stats.save()
    data = ResponseInfo.stats_info_as_json(id, value, tipe)
    return JsonResponse(data)
