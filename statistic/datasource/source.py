from statistic.models import Team, Player, Match, Stats
from datetime import datetime

class Source:
    @staticmethod
    def get_all_team_member(id):
        team_member = Team.objects.raw('''select a.id, b.id as team_member_id,d.full_name,
            d.position,
            d.alt_position,
            d.height_in_centimeter,
            d.weight_in_kilogram,
            d.overall_rating
            from statistic_team as a
            LEFT JOIN statistic_teammember as b on a.id = b.team_id
            LEFT JOIN statistic_playerattendance as c on b.player_attendance_id = c.id
            LEFT JOIN statistic_player as d on c.player_id = d.id where a.id = %s''', [id])
        return team_member
    
    @staticmethod
    def get_list_of_attendance():
        today = datetime.now().strftime('%Y-%m-%d')
        player_who_attend = Player.objects.raw(
            '''SELECT
                a.id,
                b.id as player_attendance_id,
                a.full_name,
                a.nick_name,
                a.position,
                a.alt_position,
                a.height_in_centimeter,
                a.weight_in_kilogram
                FROM statistic_player as a
                LEFT JOIN statistic_playerattendance as b on a.id = b.player_id where b.date_attendance = %s
                and b.id not in (SELECT player_attendance_id FROM statistic_teammember where team_id in (SELECT id FROM statistic_team where team_formed_date = %s))''', [today, today])
        return player_who_attend

    @staticmethod
    def get_list_of_total_stats(date):
        match_stats_total = Match.objects.raw(
            '''SELECT sm.id,
                ss.team_id,
                ((SUM(ss.score) * 2) + (SUM(ss.three_point_score) * 3) + SUM(ss.freethrow_score))  score, 
                (case when ss.team_id = st.id then st.team_name else st2.team_name end) team
                FROM statistic_match sm 
                inner join statistic_stats ss on sm.id = ss.match_id
                inner join statistic_team st on st.id = sm.home_team_id
                inner join statistic_team st2 on st2.id = sm.away_team_id 
                where sm.schedule_date = %s
                GROUP BY sm.id, ss.team_id, team
                order by sm.id DESC 
            ''', [date]
        )
        return match_stats_total

    @staticmethod
    def get_stats_of_member(id, team_id):
        stats_total = Stats.objects.raw(
            '''select 
                a.id,
                e.nick_name,
                e.back_number,
                ((SUM(a.score) * 2) + (SUM(a.three_point_score) * 3) + SUM(a.freethrow_score))  total_score,
                (SUM(a.offensive_rebound) + SUM(a.defensive_rebound))  total_rebound,
                (SUM(a.steal))  total_steal,
                (SUM(a.assists))  total_assists,
                (SUM(a.block))  total_block
                from statistic_stats a 
                inner join statistic_playerattendance b on a.player_attendance_id = b.id 
                inner join statistic_team c on a.team_id = c.id 
                inner join statistic_match d on a.match_id = d.id
                inner join statistic_player e on e.id = b.player_id 
                where d.id = %s and a.team_id = %s
                GROUP by a.id, e.full_name, e.back_number 
        ''', [id, team_id]
        )
        return stats_total
