// Call the dataTables jQuery plugin
$(document).ready(function () {
  $('#dataTable-attendance').DataTable();
});

$(document.body).on('click', '.pick-player', function () {
  var attendance_id = $(this).attr('attendance_id');
  var team_id = $(this).attr('team_id');
  var base = window.location
  var url = base.protocol + '//' + base.host + '/team/pick/' + attendance_id + '/' + team_id;
  $.ajax({
    url: url,
    type: 'GET',
    dataType: 'json',
    success: function (res) {
      var new_row = '<tr>'+
          '<td>'+res.player.full_name+'</td>'+
          '<td>'+res.player.position+'</td>'+
          '<td>'+res.player.alt_position+'</td>'+
          '<td>'+res.player.height_in_centimeter+'</td>'+
          '<td>'+res.player.weight_in_kilogram+'</td>'+
          '<td>'+res.player.overall_rating+'</td>'+
          '<td>'+
            '<p class="btn btn-large btn-danger drop-player" member_id="'+res.member_id+'" style="font-weight: bold;">'+
            'Drop '+
            '<i class="fas fa-arrow-circle-down">'+
            '</i>' +
            '</p>' +
          '</td>'+
      '</tr>';
      $('#dataTable-member').find('tbody:last').append(new_row);
    }
  });
  $(this).parent().parent().remove();
})

$(document.body).on('click', '.drop-player', function () {
  var member_id = $(this).attr('member_id');
  var base = window.location
  var url = base.protocol + '//' + base.host + '/team/drop/' + member_id;
  $.ajax({
    url: url,
    type: 'GET',
    dataType: 'json', // added data type
    success: function (res) {
      var new_row = '<tr>'+
          '<td>'+res.player.full_name+'</td>'+
          '<td>'+res.player.position+'</td>'+
          '<td>'+res.player.alt_position+'</td>'+
          '<td>'+res.player.height_in_centimeter+'</td>'+
          '<td>'+res.player.weight_in_kilogram+'</td>'+
          '<td>'+res.player.overall_rating+'</td>'+
          '<td>'+
            '<p class="btn btn-large btn-info pick-player" attendance_id="'+res.attendance_id+'" team_id="'+res.team.id+'" style="font-weight: bold;">'+
            'Pick '+
            '<i class="fas fa-arrow-circle-up">'+
            '</i>' +
            '</p>' +
          '</td>'+
      '</tr>';
      $('#dataTable-attendance').find('tbody').prepend(new_row);
    }
  });
  $(this).parent().parent().remove();
})