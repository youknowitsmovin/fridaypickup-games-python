$('#dataTable-attendance').DataTable({
    "paging": false
});

$('#submit-delete').hide()
$('#cancel-all').hide()
$('.check-delete').hide()
$('.checkbox-delete').hide()


$(document.body).on('click', '#delete-all', function () {
    $(this).hide()
    $('#submit-delete').toggle()
    $('#cancel-all').toggle()
    $('.check-delete').toggle()
    $('.checkbox-delete').toggle()
})

$(document.body).on('click', '#cancel-all', function () {
    $(this).hide()
    $('#delete-all').toggle()
    $('#submit-delete').toggle()
    $('.check-delete').toggle()
    $('.checkbox-delete').toggle()
})

$(document.body).on('click', '#select-delete', function() {
    var c = this.checked
    $(':checkbox').prop('checked', c)
})