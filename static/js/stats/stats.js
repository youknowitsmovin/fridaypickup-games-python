const SCORE = 1;
const SCORE_ATTEMPT = 2;
const THREE_POINT_SCORE = 3;
const THREE_POINT_SCORE_ATTEMPT = 4;
const ASSISTS = 5;
const STEAL = 6;
const BLOCK = 7;
const TURNOVER = 8;
const OFFENSIVE_REBOUND = 9;
const DEFENSIVE_REBOUND = 10;
const FREETHROW_SCORE = 11;
const FREETHROW_ATTEMPT = 12;
const FOUL = 13;

function getSelector(id, tipe) {
    var selector = '#';
    switch (tipe) {
        case SCORE:
            selector = selector + 'score_' + id;
            break;
        case SCORE_ATTEMPT:
            selector = selector + 'score_attempt_' + id;
            break;
        case THREE_POINT_SCORE:
            selector = selector + 'three_point_score_' + id;
            break;
        case THREE_POINT_SCORE_ATTEMPT:
            selector = selector + 'three_point_score_attempt_' + id;
            break;
        case ASSISTS:
            selector = selector + 'assists_' + id;
            break;
        case STEAL:
            selector = selector + 'steal_' + id;
            break;
        case BLOCK:
            selector = selector + 'block_' + id;
            break;
        case TURNOVER:
            selector = selector + 'turnover_' + id;
            break;
        case OFFENSIVE_REBOUND:
            selector = selector + 'offensive_rebound_' + id;
            break;
        case DEFENSIVE_REBOUND:
            selector = selector + 'defensive_rebound_' + id;
            break;
        case FREETHROW_SCORE:
            selector = selector + 'freethrow_score_' + id;
            break;
        case FREETHROW_ATTEMPT:
            selector = selector + 'freethrow_attempt_' + id;
            break;
        case FOUL:
            selector = selector + 'foul_' + id;
            break;
        default:
            selector = '-';
            break;
    }
    return selector;
}

$(document).ready(function () {
    $('#dataTable-match').DataTable({
        paging: false,
        scrollX: true,
        fixedColumns: {
            leftColumns: 2,
            rightColumns: 0
        }
    });

    $('.dataTables_filter').remove();
    $('.dataTables_info').remove();
});

$(document.body).on('click', '.score-plus', function () {
    var id = $(this).attr('stats_id');
    var tipe = $(this).attr('stats_tipe');
    var base = window.location
    var url = base.protocol + '//' + base.host + '/match/stats/increase/' + id + '/' + tipe;

    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function (res) {
            var selector = getSelector(res.id, res.type);
            $(selector).val(res.value);
        }
    });
});

$(document.body).on('click', '.score-minus', function () {
    var id = $(this).attr('stats_id');
    var tipe = $(this).attr('stats_tipe');
    var base = window.location
    var url = base.protocol + '//' + base.host + '/match/stats/decrease/' + id + '/' + tipe;

    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function (res) {
            var selector = getSelector(res.id, res.type);
            $(selector).val(res.value);
        }
    });
});