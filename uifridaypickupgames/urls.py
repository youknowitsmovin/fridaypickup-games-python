"""uifridaypickupgames URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from statistic import views

app_name = 'pickup-games'
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='dashboard'),
    path('player/', include([
        path('', views.upload_player, name='dashboard.player'),
        path('list', views.list_player, name='dashboard.player.list'),
        path('<int:id>', views.edit_player, name='dashboard.player.edit')
    ])),
    path('attendance/', include([
        path('cancel/<int:id>', views.cancel_attendance, name='dashboard.cancel'),
        path('upload/', views.upload_attendance, name='dashboard.upload'),
        path('cancel/bulk', views.bulk_cancel_attendance, name='dashboard.bulk.cancel')
    ])),
    # path('cancel/attendance/<int:id>', views.cancel_attendance, name='dashboard.cancel'),
    path('list/setting/', views.list_setting, name='dashboard.list.settings'),
    # team url
    path('team/', include([
        path('list/', views.list_team, name='dashboard.list.team'),
        path('detail/<int:id>', views.team_detail, name='dashboard.detail.team'),
        path('create/', views.create_team, name='dashboard.create.team'),
        path('random/<int:id>', views.random_pick, name='dashboard.random.pick'),
        path('edit/<int:id>', views.edit_team, name='dashboard.edit.team'),
        #ajax request
        path('pick/<int:attendance_id>/<int:team_id>', views.pick_player, name='dashboard.pick'),
        path('drop/<int:id>', views.drop_player, name='dashboard.drop'),
    ])),
    path('match/',include([
        path('list/', views.list_match, name='dashboard.list.match'),
        path('pick/<int:id>', views.match_pick_team, name='dashboard.pick.match'),
        path('pick/stats/<int:id>/<int:flag_team>', views.match_pick_stats, name='dashboard.pick.stats'),
        path('finish/<int:id>/<int:team_id>/<int:flag>', views.finish_game, name='dashboard.finish.game'),
        #ajax request
        path('stats/increase/<int:id>/<int:stats_id>', views.increase_stats, name='dashboard.increase.stats'),
        path('stats/decrease/<int:id>/<int:stats_id>', views.decrease_stats, name='dashboard.decrease.stats'),
    ]))
]
